#!/bin/bash

DIRNAME="$(dirname "$0")"

if [ -z "$DISK" ]; then
    RAMDISK=${RAMDISK:-/dev/ram1}

    # Create a ramdisk device:
    [ -b "$RAMDISK" ] || mknod -m 660 "$RAMDISK" b 1 1 || exit 1

    DISK="$RAMDISK"
fi

MODES='nocrypt dm-integrity' "$DIRNAME/test_speed.sh" raw:"$DISK"
