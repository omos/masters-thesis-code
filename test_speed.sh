#!/bin/bash

DM_DEVICE_NAME="${DM_DEVICE_NAME:-speed-test}"
DD_BLOCK_SIZE=${DD_BLOCK_SIZE:-$((32 * 1024 * 1024))}
SLEEP_SECS=${SLEEP_SECS:-0}
BENCH_MIN_DATA=${BENCH_MIN_DATA:-$((1024 * 1024 * 1024))}
BENCH_MIN_SECS=${BENCH_MIN_SECS:-2}
CRYPTSETUP="${CRYPTSETUP:-cryptsetup}"
CRYPTSETUP_FLAGS="${CRYPTSETUP_FLAGS:-}"
TEMP_FILE_SIZE=${TEMP_FILE_SIZE:-$((128 * 1024 * 1024))}
MODES="${MODES:-nocrypt dm-crypt dm-integrity luks2}"
OUTPUT_MODE=${OUTPUT_MODE:-table}

##### Cleanup functions #####

CLEANUP_STACK=()

function cleanup_return() {
    eval "${CLEANUP_STACK[$((${#FUNCNAME[@]} - 1))]:-}"
    unset CLEANUP_STACK[$((${#FUNCNAME[@]} - 1))]
}

function cleanup_exit() {
    for i in $(echo -n "${!CLEANUP_STACK[@]}" | tr ' ' '\0' | sort -zr | tr '\0' ' '); do
        eval "${CLEANUP_STACK[$i]}"
    done
}

function cleanup_push() {
    CLEANUP_STACK[$((${#FUNCNAME[@]} - 1))]="$1; ${CLEANUP_STACK[$((${#FUNCNAME[@]} - 1))]:-}"
}

##### Table formatting functions #####

function table_format() {
    printf "$OUTPUT_FORMAT" "$@"
}

##### dd output parsing  #####

function dd_extract_bytes_per_sec() {
    local line=$(grep bytes)
    if [ -z "$line" ]; then
        return 1
    fi
    
    local bytes=$(echo "$line" | egrep -o '[0-9]+ bytes' | egrep -o '[0-9]+')
    local time=$(echo "$line" | egrep -o '[0-9]+(.[0-9]+)? s' | egrep -o '[0-9]+(.[0-9]+)?')
    
    bc <<< "$bytes / $time"
}

function to_mbytes_per_sec() {
    local bytes_per_sec="$1"
    
    LC_ALL=C printf "%.03f" $(bc -l <<< "$bytes_per_sec / 10^6")
}

##### Miscelaneous #####

function generate_key() {
    local key_size="$1"; shift 1
    
    if [ $key_size -eq 0 ]; then
        echo -n '-'
    else
        head -c $key_size /dev/urandom | hexdump -v -e '/1 "%02X"'
    fi
}

function clear_device() {
    local device="$1"; shift 1
    
    local size=$(blockdev --getsize64 "$device") || {
        echo "[ERROR] Unable to get size for device '$device'!" 1>&2
        return 1
    }
    
    head -c $size /dev/zero >"$device" || {
        echo "[ERROR] Unable to wipe device '$device'!" 1>&2
        return 1
    }
}

##### Generic speed test function using dd #####

function test_speed_rd() {
    local device="$1"; shift 1
    local block_size="$1"; shift 1
    
    local size=$(blockdev --getsize64 "$device") || {
        echo "[ERROR] Unable to get size for device '$device'!" 1>&2
        return 1
    }
    local passes=$(($BENCH_MIN_DATA / $size))
    if [ $passes -lt 1 ]; then
        passes=1
    fi
    if [ $size -gt $(($BENCH_MIN_DATA * 2)) ]; then
        size=$(($BENCH_MIN_DATA * 2))
    fi
    
    sleep "$SLEEP_SECS"
    
    local sum=0
    local blocks=$(($size / $block_size))
    for (( i = 0; i < $passes; i++ )); do
        local bps
        bps=$(LC_ALL=C dd if="$device" of=/dev/null \
            count=$blocks bs=$block_size iflag=direct 2>&1 | dd_extract_bytes_per_sec) \
            || return 1
        
        (( sum += $bps ))
        if [ $(($i * $size / $bps)) -ge $BENCH_MIN_SECS ]; then
            passes=$(($i + 1))
            break
        fi
    done
    
    to_mbytes_per_sec "$(( $sum / $passes ))"
}

function test_speed_wr() {
    local device="$1"; shift 1
    local block_size="$1"; shift 1
    
    local size=$(blockdev --getsize64 "$device") || {
        echo "[ERROR] Unable to get size for device '$device'!" 1>&2
        return 1
    }
    local passes=$(($BENCH_MIN_DATA / $size))
    if [ $passes -lt 1 ]; then
        passes=1
    fi
    if [ $size -gt $(($BENCH_MIN_DATA * 2)) ]; then
        size=$(($BENCH_MIN_DATA * 2))
    fi
    
    sleep "$SLEEP_SECS"
    
    local sum=0
    local blocks=$(($size / $block_size))
    for (( i = 0; i < $passes; i++ )); do
        local bps
        bps=$(LC_ALL=C dd if=/dev/zero of="$device" \
            count=$blocks bs=$block_size oflag=direct 2>&1 | dd_extract_bytes_per_sec) \
            || return 1
        
        (( sum += $bps ))
        if [ $(($i * $size / $bps)) -ge $BENCH_MIN_SECS ]; then
            passes=$(($i + 1))
            break
        fi
    done
    
    to_mbytes_per_sec "$(( $sum / $passes ))"
}

##### common speed test function #####

function test_speed_device() {
    local device="$1"; shift
    local mode="$1"; shift
    local device_name="$1"; shift
    local cipher="$1"; shift
    local key_size="$1"; shift
    local sector_size="$1"; shift
    local journal="$1"; shift
    
    local speed_rd speed_wr
    speed_rd=$(test_speed_rd "$device" "$DD_BLOCK_SIZE") || return 1
    speed_wr=$(test_speed_wr "$device" "$DD_BLOCK_SIZE") || return 1
    
    table_format "$mode" \
        "$device_name" "$cipher" "$key_size" "$sector_size" "$journal" \
        "$speed_rd" "$speed_wr"
}

##### dm-crypt functions #####

function test_speed_dmcrypt() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    local sector_size="$1"; shift 1
    local cipher="$1"; shift 1
    local key_size="$1"; shift 1
    
    trap cleanup_return RETURN
    
    local size
    size=$(blockdev --getsize64 "$device") || {
        echo "[ERROR] Unable to get size for device '$device'!" 1>&2
        return 1
    }
    local size_sectors=$(( $size / 512 ))
    
    local key="$(generate_key $key_size)"
    
    local sector_opts
    if [ -z "$sector_size" ] || [ "$sector_size" -eq 512 ]; then
        sector_opts="0"
    else
        sector_opts="1 sector_size:$sector_size"
    fi
    
    dmsetup create "$DM_DEVICE_NAME" \
        --table "0 $size_sectors crypt $cipher $key 0 $device 0 $sector_opts" \
        || return 1
    
    cleanup_push "dmsetup remove --retry '$DM_DEVICE_NAME' 2>/dev/null"
    
    test_speed_device "/dev/mapper/$DM_DEVICE_NAME" 'dm-crypt' \
        "$device_name" "$cipher" "$key_size" "$sector_size" "0"
}

##### dm-integrity functions #####

# This is a terrible hack:
function dmintegrity_read_provided_sectors() {
    local device="$1"; shift 1
    
    local ss=0
    for (( i = 0; i < 8; i++)); do
        ss=$(( $ss | (0x$(dd if="$device" skip=$((16 + $i)) bs=1 count=1 status=none | hexdump -v -e '/1 "%02x"') << ($i * 8)) ))
    done
    echo -n $ss
}

function test_speed_dmintegrity() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    local sector_size="$1"; shift 1
    local journal="$1"; shift 1
    local cipher="$1"; shift 1
    local key_size="$1"; shift 1
    local iv_size="$1"; shift 1
    local tag_size="$1"; shift 1
    
    trap cleanup_return RETURN
    
    local integrity_size=$(($iv_size + $tag_size))
    
    journal_string='J'
    if [ "$journal" -ne 1 ]; then
        journal_string='D'
    fi
    
    local size
    size=$(blockdev --getsize64 "$device") || {
        echo "[ERROR] Unable to get size for device '$device'!" 1>&2
        return 1
    }
    local sectors_per_block=$(($sector_size / 512))
    local size_sectors=$(($size / 512))
    
    local key="$(generate_key $key_size)"
    
    local sector_opts_int
    local sector_opts
    if [ -z "$sector_size" ]; then
        sector_opts_int="0"
        sector_opts="1"
    else
        sector_opts_int="1 block_size:$sector_size"
        sector_opts="2 sector_size:$sector_size"
    fi
    
    dd if=/dev/zero of="$device" bs=8M count=1 status=none || return 1
    
    for i in {1..2}; do # do this twice to work around a bug
        dmsetup create "$DM_DEVICE_NAME-integrity" \
            --table "0 8 integrity $device 0 $integrity_size $journal_string $sector_opts_int" || return 1
        
        dmsetup remove --retry "$DM_DEVICE_NAME-integrity" 2>/dev/null || return 1
    done
    
    # read maximum sector size from superblock:
    size_sectors=$(dmintegrity_read_provided_sectors "$device")
    
    dmsetup create "$DM_DEVICE_NAME-integrity" \
        --table "0 $size_sectors integrity $device 0 $integrity_size $journal_string $sector_opts_int" || return 1
    
    cleanup_push "dmsetup remove --retry '$DM_DEVICE_NAME-integrity' 2>/dev/null"
    
    dmsetup create "$DM_DEVICE_NAME" \
        --table "0 $size_sectors crypt $cipher $key 0 /dev/mapper/$DM_DEVICE_NAME-integrity 0 $sector_opts integrity:$integrity_size:aead" \
        || return 1
    
    cleanup_push "dmsetup remove --retry '$DM_DEVICE_NAME' 2>/dev/null"
    
    clear_device "/dev/mapper/$DM_DEVICE_NAME" || return 1
    
    test_speed_device "/dev/mapper/$DM_DEVICE_NAME" 'dm-integrity' \
        "$device_name" "$cipher" "$key_size" "$sector_size" "$journal"
}

##### LUKS2 functions #####

LUKS_PASSWORD="password"

function test_speed_luks2() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    local sector_size="$1"; shift 1
    local journal="$1"; shift 1
    local cipher="$1"; shift 1
    local key_size="$1"; shift 1
    local integrity="${1:-none}"; shift 1
    
    trap cleanup_return RETURN
    
    mkdir -p /run/lock || return 1
    
    dd if=/dev/zero of="$device" bs=8M count=1 status=none || return 1
    
    extra_flags=''
    if [ "$journal" -ne 1 ]; then
        extra_flags='--integrity-no-journal'
    fi
    
    "$CRYPTSETUP" luksFormat --type luks2 -q \
        $CRYPTSETUP_FLAGS $extra_flags \
        -d <(echo "$LUKS_PASSWORD") \
        -c "$cipher" -s $(($key_size * 8)) \
        -i 1 --pbkdf-memory=8 --pbkdf-parallel=1 \
        --sector-size "$sector_size" \
        --integrity "$integrity" \
        "$device" || return 1
    
    "$CRYPTSETUP" luksOpen -q \
        $CRYPTSETUP_FLAGS $extra_flags \
        -d <(echo "$LUKS_PASSWORD") \
        "$device" "$DM_DEVICE_NAME" || return 1
    
    cleanup_push "'$CRYPTSETUP' luksClose $CRYPTSETUP_FLAGS '$DM_DEVICE_NAME'"
    
    clear_device "/dev/mapper/$DM_DEVICE_NAME" || return 1
    
    local speed_rd speed_wr
    speed_rd=$(test_speed_rd "/dev/mapper/$DM_DEVICE_NAME" "$DD_BLOCK_SIZE") || return 1
    speed_wr=$(test_speed_wr "/dev/mapper/$DM_DEVICE_NAME" "$DD_BLOCK_SIZE") || return 1
    
    test_speed_device "/dev/mapper/$DM_DEVICE_NAME" 'luks2' \
        "$device_name" "$cipher/$integrity" "$key_size" "$sector_size" "$journal"
}

##### Cipher mode lists #####

function test_speed_nocrypt() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    
    test_speed_device "$device" 'nocrypt' \
        "$device_name" "<none>" "0" "512" "0"
}

function test_speed_dmcrypt_all() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    
    for sector_size in 512 4096; do
        test_speed_dmcrypt "$device_name" "$device" $sector_size 'cipher_null-ecb' 0
        test_speed_dmcrypt "$device_name" "$device" $sector_size 'aes-ecb' 32
        test_speed_dmcrypt "$device_name" "$device" $sector_size 'aes-cbc-essiv:sha256' 32
        test_speed_dmcrypt "$device_name" "$device" $sector_size 'aes-xts-plain64' 64
        test_speed_dmcrypt "$device_name" "$device" $sector_size 'aes-lrw-benbi' 32
    done
}

function test_speed_luks2_all() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    
    for sector_size in 512 4096; do
        #test_speed_luks2 "$device_name" "$device" $sector_size 0 'cipher_null-ecb' 0
        #test_speed_luks2 "$device_name" "$device" $sector_size 0 'aes-ecb' 32
        #test_speed_luks2 "$device_name" "$device" $sector_size 0 'aes-cbc-essiv:sha256' 32
        #test_speed_luks2 "$device_name" "$device" $sector_size 0 'aes-xts-plain64' 64
        #test_speed_luks2 "$device_name" "$device" $sector_size 0 'aes-lrw-benbi' 32
        for journal in 0 1; do
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'aes-gcm-plain64' 32 'aead'
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'aes-gcm-random' 32 'aead'
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'aes-ccm-random' 35 'aead'
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'chacha20-random' 32 'poly1305'
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'aes-xts-random' 64 'hmac-sha256'
            test_speed_luks2 "$device_name" "$device" $sector_size $journal 'aes-xts-random' 64 'cmac-aes'
        done
    done
}

function test_speed_dmintegrity_all() {
    local device_name="$1"; shift 1
    local device="$1"; shift 1
    
    for sector_size in 512 4096; do
        for journal in 0 1; do
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:aegis128-random' 16 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:aegis128l-random' 16 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:aegis256-random' 32 32 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:gcm(aes)-random' 16 12 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:gcm(aes)-random' 32 12 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:morus640-random' 16 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:morus1280-random' 16 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:morus1280-random' 32 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:ocb(aes)-random' 16 12 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:ocb(aes)-random' 32 12 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:rfc7539(chacha20,poly1305)-random' 32 12 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:authenc(hmac(sha256),xts(aes))-random' 64 16 32
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:authenc(hmac(sha256),xts(aes))-random' 96 16 32
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:authenc(cmac(aes),xts(aes))-random' 48 16 16
            test_speed_dmintegrity "$device_name" "$device" $sector_size $journal 'capi:authenc(cmac(aes),xts(aes))-random' 80 16 16
        done
    done
}

##### Device set-up functions #####

function test_speed_dmzero() {
    local func="$1"; shift 1
    local size="$1"; shift 1
    
    trap cleanup_return RETURN
    
    local size_sectors=$(( $size / 512 ))
    
    dmsetup create "$DM_DEVICE_NAME-zero" --table "0 $size_sectors zero" || return 1
    
    cleanup_push "dmsetup remove --retry '$DM_DEVICE_NAME-zero' 2>/dev/null"
    
    "$func" 'zero' "/dev/mapper/$DM_DEVICE_NAME-zero"
}

function test_speed_loop() {
    local func="$1"; shift 1
    local file="$1"; shift 1
    
    trap cleanup_return RETURN
    
    local loopdev
    loopdev="$(losetup --show -f "$file")" || return 1
    
    cleanup_push "losetup -d $loopdev"
    
    "$func" "loop:$file" "$loopdev"
}

function test_speed_mktemp() {
    local func="$1"; shift 1
    local size="$1"; shift 1
    local tmpdir="$1"; shift 1
    
    [ -z "$tmpdir" ] && tmpdir="$TMPDIR"
    
    local temp_file
    temp_file=$(mktemp -p "$tmpdir") || {
        echo "[ERROR] Unable to create temporary file!" 1>&2
        return 1
    }
    
    trap cleanup_return RETURN
    
    cleanup_push "rm -f $temp_file"
    
    head -c $size /dev/zero >"$temp_file" || return 1
    
    test_speed_loop "$func" "$temp_file"
}

function test_speed() {
    local func="$1"; shift 1
    
    for device in "$@"; do
        case "$device" in
            zero)
                test_speed_dmzero "$func" $((8*1024*1024*1024))
                ;;
            raw:*)
                "$func" "$device" "${device#raw:}"
                ;;
            loop:*)
                test_speed_loop "$func" "${device#loop:}"
                ;;
            loop-tmp:*)
                test_speed_mktemp "$func" $TEMP_FILE_SIZE \
                    "${device#loop-tmp:}"
                ;;
            *)
                echo "[ERROR] Invalid device specification: $device" 1>&2
                exit 1
                ;;
        esac
    done
}

##### Main script body #####

if [ $# -eq 0 ]; then
    echo "usage: $0 DEVICE..."
    echo
    echo "where each DEVICE is one of:"
    echo "  zero                 - test over dm-zero device"
    echo "  raw:<device>         - test over raw device <device>, e.g. raw:/dev/ram1"
    echo "  loop:<file>          - test over loopback over <file>"
    echo "  loop-tmp:<directory> - test over loopback over temporary file created in <directory>"
    exit 1
fi

# Parse output mode:
case "$OUTPUT_MODE" in
    'table')
        OUTPUT_FORMAT='%-15s%-30s%-45s%10s%15s%10s%10s%10s\n'
        ;;
    'csv')
        OUTPUT_FORMAT='%s,%s,"%s",%s,%s,%s,%s,%s\n'
        ;;
    *)
        echo "[ERROR] Invalid OUTPUT_MODE: $OUTPUT_MODE" 1>&2
        exit 1
esac

# Set up cleanup trap:
trap cleanup_exit EXIT

# Disable dmesg messages on console (to hide dm-integrity errors):
dmesg -D && cleanup_push 'dmesg -E'

# Start printing the speed table:
table_format 'mode' 'device' 'cipher' 'key_size' 'sector_size' 'journal' \
    'r_mbps' 'w_mbps'
for mode in $MODES; do
    case "$mode" in
        nocrypt)
            test_speed test_speed_nocrypt "$@"
            ;;
        dm-crypt)
            test_speed test_speed_dmcrypt_all "$@"
            ;;
        dm-integrity)
            test_speed test_speed_dmintegrity_all "$@"
            ;;
        luks2)
            test_speed test_speed_luks2_all "$@"
            ;;
        *)
            echo "[ERROR] Invalid mode: $mode" 1>&2
    esac
done
