#!/usr/bin/Rscript

ALGS_DIRECT <- c(
    'aegis128-128',
    'aegis128l-128',
    'aegis256-256',
    'gcm(aes)-128',
    'gcm(aes)-256',
    'morus640-128',
    'morus1280-256',
    'ocb(aes)-128',
    'ocb(aes)-256',
    'rfc7539(chacha20,poly1305)-256')

COLORS_DIRECT <- c(
    '#FF0000', '#770000', '#440000',
    '#00FF00', '#007700',
    '#0000FF', '#000077',
    '#FFFF00', '#777700',
    '#000000')

EXTENSIONS <- c('all', 'sse2', 'generic')

require(ggplot2)
require(scales)

NS_PER_SEC <- 1000 * 1000 * 1000

theme_set(theme_gray(base_size = 10))

GRAPH_WIDTH_CM <- 10
GRAPH_HEIGHT_CM <- 6

save_graph <- function(name, plot) {
  ggsave(paste0(name, '.pdf'), plot, width = GRAPH_WIDTH_CM, height = GRAPH_HEIGHT_CM, scale = 2.3, units = 'cm')
}

machines <- commandArgs(trailingOnly = TRUE)

### Load data:
data_direct <- data.frame()

for (extensions in EXTENSIONS) {
    for (machine in machines) {
        file <- paste0('bench-direct-', machine, '-', extensions, '-local.csv')
        file_data <- read.csv(file)
        data_direct <- rbind(data_direct, data.frame(
            Extensions=extensions, Machine=machine, Algorithm.name=file_data$alg,
            Key.bits=file_data$key_bits, AD.bytes=file_data$ad_bytes,
            Data.bytes=file_data$data_bytes, Direction='Encryption',
            Nanosecs=file_data$enc_ns))
        data_direct <- rbind(data_direct, data.frame(
            Extensions=extensions, Machine=machine, Algorithm.name=file_data$alg,
            Key.bits=file_data$key_bits, AD.bytes=file_data$ad_bytes,
            Data.bytes=file_data$data_bytes, Direction='Decryption',
            Nanosecs=file_data$dec_ns))
    }
}

data_direct$Algorithm <- paste0(data_direct$Algorithm.name, '-', data_direct$Key.bits)
data_direct$Has.AD <- ifelse(data_direct$AD.bytes != 0, 'With AD', 'No AD')
data_direct$Mbps <- 1000 * (data_direct$AD.bytes + data_direct$Data.bytes) / data_direct$Nanosecs
data_direct$Ns.per.byte <- data_direct$Nanosecs / (data_direct$AD.bytes + data_direct$Data.bytes)

data_direct <- data_direct[data_direct$Algorithm %in% ALGS_DIRECT,]

make_plot_direct <- function(data, extensions, machine) {
    save_graph(paste0('plot-speed-direct-', machine, '-', extensions),
        ggplot(data, aes(x=factor(Data.bytes), y=Mbps, fill=Algorithm)) +
            geom_bar(colour="black", stat="identity", position=position_dodge()) +
            scale_y_continuous(labels=comma, limits=c(0, NA)) +
            #scale_fill_brewer(palette="Spectral") +
            scale_fill_manual(values=COLORS_DIRECT) +
            facet_grid(Direction~Has.AD) +
            xlab('Message size (bytes)') + ylab('Speed (MB/s)'))
    save_graph(paste0('plot-nspb-direct-', machine, '-', extensions),
        ggplot(data, aes(x=factor(Data.bytes), y=Ns.per.byte, fill=Algorithm)) +
            geom_bar(colour="black", stat="identity", position=position_dodge()) +
            scale_y_continuous(labels=comma, limits=c(0, NA)) +
            #scale_fill_brewer(palette="Spectral") +
            scale_fill_manual(values=COLORS_DIRECT) +
            facet_grid(Direction~Has.AD) +
            xlab('Message size (bytes)') + ylab('Nanoseconds per byte'))
}

for (extensions in EXTENSIONS) {
    for (machine in machines) {
        filter <- data_direct$Extensions == extensions & data_direct$Machine == machine
        make_plot_direct(data_direct[filter,], extensions, machine)
    }
}
