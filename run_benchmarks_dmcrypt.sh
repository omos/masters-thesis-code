#!/bin/bash

DIRNAME="$(dirname "$0")"

MACHINE=${MACHINE:-ramdisk}

"$DIRNAME/build_modules.sh" || exit 1

sudo env OUTPUT_MODE=csv "$DIRNAME/local_bench_dmcrypt.sh" | \
    tee "$DIRNAME/bench-data/bench-dmcrypt-$MACHINE-local.csv"
