#!/bin/bash

DIRNAME="$(dirname "$0")"

# Start udev:
/lib/systemd/systemd-udevd --daemon || exit 1
udevadm trigger --action=add || exit 1
udevadm settle || exit 1
mkdir -p /dev/pts || exit 1
mount -n -t devpts devpts /dev/pts 2> /dev/null || true

"$DIRNAME/with_modules.sh" "$DIRNAME/do_bench_dmcrypt.sh" || exit 1
