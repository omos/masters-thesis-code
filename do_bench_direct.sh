#!/bin/bash

DIRNAME="$(dirname "$0")"

OUTPUT_MODE="${OUTPUT_MODE:-table}"

function load_bench() {
    if [ "$OUTPUT_MODE" == 'table' ]; then
        (cd "$DIRNAME/linux-crypto-bench" && CRYPTO_BENCH_TYPES=aead ./load-human.sh) || return 1
    elif [ "$OUTPUT_MODE" == 'csv' ]; then
        (cd "$DIRNAME/linux-crypto-bench" && CRYPTO_BENCH_TYPES=aead ./load-csv.sh) || return 1
    else
        echo "ERROR: Invalid output mode: '$OUTPUT_MODE'!" 1>&2
        return 1
    fi
}

function unload_bench() {
    (cd "$DIRNAME/linux-crypto-bench" && ./unload.sh)
}

trap unload_bench EXIT

load_bench || { dmesg | less +G; exit 1; }

dmesg -l emerg -t
