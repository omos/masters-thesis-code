#!/bin/bash

DIRNAME="$(dirname "$0")"

MACHINE=${MACHINE:-laptop}

"$DIRNAME/build_modules.sh" || exit 1
for ext in all sse2 generic; do
     sudo env EXTENSIONS=$ext OUTPUT_MODE=csv "$DIRNAME/local_bench_direct.sh" | \
        tee "$DIRNAME/bench-data/bench-direct-$MACHINE-$ext-local.csv"
done
