#!/bin/bash

MAKE_ARGS=''
if [ -n "$KERNEL_VERSION" ]; then
    MAKE_ARGS=KERNEL_VERSION="$KERNEL_VERSION"
fi

SUBDIRS='linux-crypto-bench linux-crypto-aegis linux-crypto-morus linux-crypto-ocb'
for dir in $SUBDIRS; do
    (cd "$dir" && make $MAKE_ARGS) || exit 1
done
