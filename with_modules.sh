#!/bin/bash

DIRNAME="$(dirname "$0")"

#EXTENSIONS=${EXTENSIONS:-'all'}

function load_modules() {
    if [ "$EXTENSIONS" == 'all' ]; then
        (cd "$DIRNAME/linux-crypto-aegis" && ./load-all.sh) || return 1
        (cd "$DIRNAME/linux-crypto-morus" && ./load-all.sh) || return 1
        (cd "$DIRNAME/linux-crypto-ocb"   && ./load-all.sh) || return 1
    elif [ "$EXTENSIONS" == 'sse2' ]; then
        (cd "$DIRNAME/linux-crypto-aegis" && ./load-all.sh)  || return 1
        (cd "$DIRNAME/linux-crypto-morus" && ./load-sse2.sh) || return 1
        (cd "$DIRNAME/linux-crypto-ocb"   && ./load-all.sh)  || return 1
    elif [ "$EXTENSIONS" == 'generic' ]; then
        (cd "$DIRNAME/linux-crypto-aegis" && ./load-generic.sh) || return 1
        (cd "$DIRNAME/linux-crypto-morus" && ./load-generic.sh) || return 1
        (cd "$DIRNAME/linux-crypto-ocb"   && ./load-generic.sh) || return 1
    else
        echo "ERROR: Invalid extension specifier: '$EXTENSIONS'!" 1>&2
        return 1
    fi
}

function unload_modules() {
    (cd "$DIRNAME/linux-crypto-aegis" && ./unload.sh)
    (cd "$DIRNAME/linux-crypto-morus" && ./unload.sh)
    (cd "$DIRNAME/linux-crypto-ocb"   && ./unload.sh)
}

trap unload_modules EXIT

load_modules || { dmesg | less +G; exit 1; }

"$@"
