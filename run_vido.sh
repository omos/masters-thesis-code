#!/bin/bash

DIRNAME="$(dirname "$0")"

bash "$DIRNAME/build_modules.sh" || exit 1

vido --kvm --qemu-9p-workaround --mem 1024M --kernel-version "$KERNEL_VERSION" "$@"
